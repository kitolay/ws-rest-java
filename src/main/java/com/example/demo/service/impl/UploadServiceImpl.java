package com.example.demo.service.impl;

import com.example.demo.service.UploadFileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class UploadServiceImpl implements UploadFileService {

    @Override
    public void upload(MultipartFile file) throws IOException {
        file.transferTo(new File("C:\\Windows\\Temp\\" + file.getOriginalFilename()));
    }
}
