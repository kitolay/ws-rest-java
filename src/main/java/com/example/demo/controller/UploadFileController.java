package com.example.demo.controller;

import com.example.demo.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class UploadFileController {

    @Autowired
    UploadFileService uploadFileservice;

    @PostMapping("/upload")
    public void upload(@RequestParam MultipartFile file) throws IOException {
        uploadFileservice.upload(file);
    }
}
